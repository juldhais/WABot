﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace WABot
{
    public class WAEngine
    {
        private const string QR_CODE_XPATH = "//img[@alt='Scan me!']";
        private const string INPUT_SEARCH_XPATH = "//input[@title='Search or start new chat']";
        private const string INPUT_CHAT_XPATH = "//div[@class='_2S1VP copyable-text selectable-text']";

        private static IWebDriver webDriver = null;

        public void Start()
        {
            webDriver = new ChromeDriver(new ChromeOptions { LeaveBrowserRunning = false });
            webDriver.Url = "https://web.whatsapp.com";

            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            webDriver.Navigate();
        }

        public bool IsLoggedIn()
        {
            try
            {
                if (webDriver.FindElement(By.XPath(INPUT_SEARCH_XPATH)) == null) return false;
                if (webDriver.FindElement(By.XPath(INPUT_CHAT_XPATH)) == null) return false;
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        public void Send(string phoneNumber, string message)
        {
            try
            {
                var inputSearch = webDriver.FindElement(By.XPath(INPUT_SEARCH_XPATH));
                inputSearch.SendKeys(phoneNumber);
                inputSearch.SendKeys(Keys.Enter);

                var inputMessage = webDriver.FindElement(By.XPath(INPUT_CHAT_XPATH));
                inputMessage.SendKeys(message);
                inputMessage.SendKeys(Keys.Enter);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public List<string> GetAllContact()
        {
            var buttonNewChat = webDriver.FindElement(By.XPath("//div[@title='New chat']"));
            buttonNewChat.Click();

            Thread.Sleep(1000);
            //_3TEwt
            //_1wjpf

            var list_3TEwt = webDriver.FindElements(By.XPath("//span[@class='_3TEwt']"));

            var result = new List<string>();
            foreach (var item in list_3TEwt)
            {
                var _1wjpf = item.FindElement(By.ClassName("_1wjpf"));
                result.Add(_1wjpf.Text);
            }

            var buttonBack = webDriver.FindElement(By.ClassName("_1aTxu"));
            if (buttonBack != null) buttonBack.Click();

            return result;
        }

        public void Stop()
        {
            webDriver.Quit();
            webDriver.Dispose();
        }
    }
}
