﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace WABot
{
    public partial class MainForm : Form
    {
        WAEngine waEngine;

        public MainForm()
        {
            waEngine = new WAEngine();

            InitializeComponent();

            buttonLogin.Click += ButtonLogin_Click;
            buttonLogout.Click += ButtonLogout_Click;
            buttonSend.Click += ButtonSend_Click;
            buttonGetAllContact.Click += ButtonGetAllContact_Click;

            SetLoginState(false);
        }

        private void SetLoginState(bool login)
        {
            buttonLogin.Enabled = !login;
            buttonLogout.Enabled = login;
            buttonSend.Enabled = login;
            buttonGetAllContact.Enabled = login;
        }

        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            UseWaitCursor = true;

            waEngine.Start();

            SetLoginState(true);

            UseWaitCursor = false;
        }

        private void ButtonLogout_Click(object sender, EventArgs e)
        {
            waEngine.Stop();
            SetLoginState(false);
        }

        private void ButtonSend_Click(object sender, EventArgs e)
        {
            try
            {
                var message = textMessage.Text;
                var listMessage = message.Split('\n');
                foreach (var item in listMessage)
                {
                    waEngine.Send(textPhoneNumber.Text, item);
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void ButtonGetAllContact_Click(object sender, EventArgs e)
        {
            var list = waEngine.GetAllContact();
            var text = "";
            foreach (var item in list)
            {
                text += item + "\n";
            }

            MessageBox.Show(text);
        }
    }
}
